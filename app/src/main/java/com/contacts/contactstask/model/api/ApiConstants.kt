package com.contacts.contactstask.model.api

object ApiConstants {
    const val REST_ENDPOINT = "https://file.wowapp.me/owncloud/index.php/s/F5WttwCODi1z3oo/"
    const val GET_CONTACTS = "download?path=%2F&files=contacts.json"

    const val STATUS_ONLINE = "online"
    const val STATUS_AWAY = "away"
    const val STATUS_BUSY = "busy"
    const val STATUS_PENDING = "pending"
    const val STATUS_CALL_FORWARDING = "callforwarding"
}