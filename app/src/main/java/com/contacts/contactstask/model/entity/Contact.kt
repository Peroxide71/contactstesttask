package com.contacts.contactstask.model.entity


data class Contact (var firstName: String?,
                    var lastName: String?,
                    var statusIcon: String?,
                    var statusMessage: String?): ListItem(ListItem.TYPE_CONTACT)