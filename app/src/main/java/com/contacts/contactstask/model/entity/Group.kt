package com.contacts.contactstask.model.entity


class Group (var groupName: String,
             var people: List<Contact>): ListItem(ListItem.TYPE_GROUP)