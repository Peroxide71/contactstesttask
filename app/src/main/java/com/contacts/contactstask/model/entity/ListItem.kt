package com.contacts.contactstask.model.entity

abstract class ListItem(var type: Int) {

    companion object {
        val TYPE_GROUP = 0
        val TYPE_CONTACT = 1
    }
}