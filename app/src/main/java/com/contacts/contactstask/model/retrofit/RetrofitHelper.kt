package com.contacts.contactstask.model.retrofit

import com.contacts.contactstask.model.api.ApiConstants
import com.contacts.contactstask.model.api.ApiConstants.REST_ENDPOINT
import com.contacts.contactstask.model.usecase.GroupsRestResponse
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET

object RetrofitHelper {
    private val loggingInterceptor = HttpLoggingInterceptor()

    private val retrofit = Retrofit.Builder()
        .baseUrl(REST_ENDPOINT)
        .client(
            OkHttpClient.Builder().addInterceptor(
                loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            ).build()
        )
        //Adapters and converter factories required to put Retrofit calls to coroutines.
        .addConverterFactory(GsonConverterFactory.create())
        .addConverterFactory(MoshiConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
    val contactsAPI: ContactsApi? = retrofit.create(ContactsApi::class.java)

    interface ContactsApi {

        @GET(ApiConstants.GET_CONTACTS)
        fun fetchContactsAsync(): Deferred<Response<GroupsRestResponse>>
    }
}