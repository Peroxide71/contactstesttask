package com.contacts.contactstask.model.usecase

import com.contacts.contactstask.model.entity.Group
import com.contacts.contactstask.model.retrofit.RetrofitHelper
import com.contacts.contactstask.model.usecase.response.GroupsResponse
import kotlinx.coroutines.*

object ContactsUseCase {
    var coroutineScope: CoroutineScope? = null

    fun fetchContactsGroupsListAsync(): Deferred<GroupsResponse>? {
        coroutineScope?.cancel()
        coroutineScope = CoroutineScope(Dispatchers.IO)
//        Executing asynchronous REST request
        return coroutineScope?.async {
            var groups: List<Group> = listOf()
            try {
                var errorMessage: String? = null
                val response = RetrofitHelper.contactsAPI?.fetchContactsAsync()?.await()
                response?.run{
                    if (isSuccessful) {
                        groups = body()?.groups ?: listOf()
                    } else {
                        errorMessage = message()
                    }
                }
                GroupsResponse(response?.isSuccessful ?: false, groups, errorMessage)
            } catch (e: Exception) {
                e.printStackTrace()
                GroupsResponse(false, groups, e.message)
            }
        }
    }
}