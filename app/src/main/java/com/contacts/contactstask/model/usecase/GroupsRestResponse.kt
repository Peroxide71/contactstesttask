package com.contacts.contactstask.model.usecase

import com.contacts.contactstask.model.entity.Group

class GroupsRestResponse(val groups: List<Group>)