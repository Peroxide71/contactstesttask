package com.contacts.contactstask.model.usecase.response

import com.contacts.contactstask.model.entity.Group

class GroupsResponse (val isSuccess: Boolean = true,
                      val data: List<Group>,
                      val errorMessage: String? = null)