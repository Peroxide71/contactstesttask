package com.contacts.contactstask.view

import android.app.Application
import com.contacts.contactstask.view.ioc.ResourcesProviderImpl
import com.contacts.contactstask.viewmodel.ioc.ResourceProviderResolver

class ContactsApp: Application(){
    override fun onCreate() {
        super.onCreate()
        val resourcesProviderImpl = ResourcesProviderImpl(applicationContext)
        ResourceProviderResolver.registerResouceProvider(resourcesProviderImpl)
    }
}