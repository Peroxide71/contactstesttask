package com.contacts.contactstask.view.activity

import androidx.lifecycle.Observer
import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import com.contacts.contactstask.R
import com.contacts.contactstask.BR
import com.contacts.contactstask.databinding.ActivityMainBinding
import com.contacts.contactstask.model.entity.Group
import com.contacts.contactstask.viewmodel.MainActivityViewModel

class MainActivity : AppCompatActivity() {
    private val viewModel: MainActivityViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding = DataBindingUtil
            .setContentView(this@MainActivity, R.layout.activity_main)
        binding.setVariable(BR.screen, viewModel)
//        In case of device rotation no request should be made.
//        Just reusing data stored in ViewModel.
        viewModel.getScreenData(savedInstanceState == null)?.observe(this, object : Observer<List<Group>> {
            override fun onChanged(t: List<Group>?) {
                binding.setVariable(BR.screen, viewModel)
                viewModel.errorText?.let {
                    Toast.makeText(
                        applicationContext,
                        it, Toast.LENGTH_LONG
                    ).show()
                }
            }
        })
    }
}
