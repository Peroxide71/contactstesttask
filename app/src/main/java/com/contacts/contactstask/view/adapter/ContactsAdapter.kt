package com.contacts.contactstask.view.adapter

import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.contacts.contactstask.R
import com.contacts.contactstask.databinding.ContactLayoutBinding
import com.contacts.contactstask.databinding.GroupItemBinding
import com.contacts.contactstask.model.api.ApiConstants
import com.contacts.contactstask.model.entity.Contact
import com.contacts.contactstask.model.entity.Group
import com.contacts.contactstask.model.entity.ListItem
import com.contacts.contactstask.viewmodel.databinding.items_viewmodel.ContactViewModel
import com.contacts.contactstask.viewmodel.databinding.items_viewmodel.GroupViewModel

class ContactsAdapter: androidx.recyclerview.widget.RecyclerView.Adapter<ContactsAdapter.ContactViewHolder>() {

    private var listData: MutableList<ListItem> = mutableListOf()
    var layoutInflater: LayoutInflater? = null
    var data: List<Group> = listOf()
    set(items) {
        listData.clear()
        items.iterator().forEach {
//            Flattening the source data to display multiple view types on the same level.
            it.type = ListItem.TYPE_GROUP
            listData.add(it)
            it.people.iterator().forEach {
                it.type = ListItem.TYPE_CONTACT
                listData.add(it)
            }
        }
        field = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        if(layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }
        when (viewType) {
            ListItem.TYPE_GROUP -> {
                val binding: GroupItemBinding = DataBindingUtil.inflate(layoutInflater!!,
                    R.layout.group_item,
                    parent,
                    false)

                return ContactViewHolder(binding)
            }
            ListItem.TYPE_CONTACT -> {
                val binding: ContactLayoutBinding = DataBindingUtil.inflate(layoutInflater!!,
                    R.layout.contact_layout,
                    parent,
                    false)
                return ContactViewHolder(binding)
            }
            else -> {
                val binding: ContactLayoutBinding = DataBindingUtil.inflate(layoutInflater!!,
                    R.layout.contact_layout,
                    parent,
                    false)
                return ContactViewHolder(binding)
            }
        }
    }

    override fun getItemCount(): Int = listData.size

    override fun getItemViewType(position: Int): Int {
        return listData[position].type
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        val dataItem = listData[position]
        if(dataItem is Group) {
            val viewModel = GroupViewModel(dataItem.groupName)
            holder.groupBinding?.group = viewModel
        } else if(dataItem is Contact) {
            val viewModel = ContactViewModel(
                dataItem.firstName?.plus(" ")?.plus(dataItem.lastName) ?: "",
                getStatusIcon(dataItem),
                dataItem.statusMessage ?: "",
                R.drawable.contacts_list_avatar_unknown
            )
            holder.contactBinding?.item = viewModel
        }
    }



    private fun getStatusIcon(dataItem: Contact): Int {
        return when (dataItem.statusIcon) {
            ApiConstants.STATUS_ONLINE -> R.drawable.contacts_list_status_online
            ApiConstants.STATUS_AWAY -> R.drawable.contacts_list_status_away
            ApiConstants.STATUS_BUSY -> R.drawable.contacts_list_status_busy
            ApiConstants.STATUS_PENDING -> R.drawable.contacts_list_status_pending
            ApiConstants.STATUS_CALL_FORWARDING -> R.drawable.contacts_list_call_forward
            else -> R.drawable.contacts_list_status_pending
        }
    }


    class ContactViewHolder : androidx.recyclerview.widget.RecyclerView.ViewHolder {
        var groupBinding: GroupItemBinding? = null
        var contactBinding: ContactLayoutBinding? = null

        constructor(binding: GroupItemBinding) : super(binding.root) {
            groupBinding = binding
        }

        constructor(binding: ContactLayoutBinding) : super(binding.root) {
            contactBinding = binding
        }
    }
}