package com.contacts.contactstask.view.ioc

import android.content.Context
import com.contacts.contactstask.viewmodel.ioc.StringResourceProvider

class ResourcesProviderImpl(val app: Context):
    StringResourceProvider {
    override fun getStringById(id: Int): String? {
        return app.applicationContext.getString(id)
    }
}