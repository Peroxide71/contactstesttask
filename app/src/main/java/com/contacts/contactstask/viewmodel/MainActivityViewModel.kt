package com.contacts.contactstask.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.viewModelScope
import com.contacts.contactstask.R
import com.contacts.contactstask.model.entity.Group
import com.contacts.contactstask.model.usecase.ContactsUseCase
import com.contacts.contactstask.model.usecase.response.GroupsResponse
import com.contacts.contactstask.viewmodel.ioc.ResourceProviderResolver
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainActivityViewModel : ViewModel() {
    private var screenLiveData: MutableLiveData<List<Group>>? = null
    var loadedData: List<Group> = listOf()
    var isLoaded = ObservableBoolean(false)
    var loadingMessage = ObservableField<CharSequence>()
    var errorText: String? = null

    fun getScreenData(reload: Boolean): MutableLiveData<List<Group>>? {
        if (screenLiveData == null) {
            screenLiveData = MutableLiveData()
        }
        isLoaded.set(false)
        loadingMessage.set(ResourceProviderResolver.resolveResourceProvider().getStringById(R.string.loading_message))
        loadData(reload)
        return screenLiveData
    }

    private fun loadData(reload: Boolean) {
        viewModelScope.launch (Dispatchers.Main){
//            Thanks to coroutines we don't need to care about suspend calls here.
            val groupsResponse = if(reload) {
                ContactsUseCase.fetchContactsGroupsListAsync()?.await()
            } else {
                GroupsResponse(true, loadedData)
            }
            groupsResponse?.run {
                val message = if(isSuccess) null else ResourceProviderResolver.resolveResourceProvider().getStringById(R.string.error_message)
                loadingMessage.set(message)
                isLoaded.set(true)
                screenLiveData?.postValue(data)
                errorText = errorMessage
                loadedData = data
            }
        }
    }
}