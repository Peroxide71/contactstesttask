package com.contacts.contactstask.viewmodel.databinding

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import com.contacts.contactstask.model.entity.Group
import com.contacts.contactstask.view.adapter.ContactsAdapter

object BindingAdapter {

//    Adapter methods to bind specific values to views.
    @JvmStatic
    @BindingAdapter("data")
    fun setData(recyclerView: androidx.recyclerview.widget.RecyclerView, data: List<Group>?) {
        val recyclerViewAdapter = recyclerView.adapter
        if(recyclerViewAdapter != null && recyclerViewAdapter is ContactsAdapter){
            recyclerViewAdapter.data = data ?: listOf()
        } else {
            val adapter = ContactsAdapter()
            recyclerView.layoutManager =
                androidx.recyclerview.widget.LinearLayoutManager(recyclerView.context)
            recyclerView.adapter = adapter
        }
    }

    @JvmStatic
    @BindingAdapter("android:src")
    fun setImage(view: ImageView, res: Int) {
        view.setImageResource(res)
    }

    @JvmStatic
    @BindingAdapter("android:visibility")
    fun setVisibility(view: View, value: Boolean) {
        view.visibility = if (value) View.VISIBLE else View.GONE
    }
}