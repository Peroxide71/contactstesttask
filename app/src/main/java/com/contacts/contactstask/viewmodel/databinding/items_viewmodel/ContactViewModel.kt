package com.contacts.contactstask.viewmodel.databinding.items_viewmodel

class ContactViewModel (var firstName: String,
                        var statusIcon: Int,
                        var statusMessage: String,
                        var avatar: Int)