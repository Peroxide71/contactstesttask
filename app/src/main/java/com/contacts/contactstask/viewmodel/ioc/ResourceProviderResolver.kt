package com.contacts.contactstask.viewmodel.ioc

object ResourceProviderResolver {
    private lateinit var resourceProvider: StringResourceProvider

    fun registerResouceProvider(provider: StringResourceProvider) {
        resourceProvider = provider
    }

    fun resolveResourceProvider(): StringResourceProvider = resourceProvider
}