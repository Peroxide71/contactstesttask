package com.contacts.contactstask.viewmodel.ioc

interface StringResourceProvider {
    fun getStringById(id: Int): String?
}